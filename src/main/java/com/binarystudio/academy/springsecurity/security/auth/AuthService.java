package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserRepository;
import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Optional;

@Log4j2
@Service
public class AuthService {
	private final UserService userService;
	private final JwtProvider jwtProvider;
	private final PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.jwtProvider = jwtProvider;
		this.passwordEncoder = passwordEncoder;
	}

	public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
		var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
		if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}
		return AuthResponse.of(jwtProvider.generateToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
	}

	private boolean passwordsDontMatch(String rawPw, String encodedPw) {
		return !passwordEncoder.matches(rawPw, encodedPw);
	}

	public AuthResponse performRegistration(RegistrationRequest registrationRequest) {
		if (emailExist(registrationRequest.getEmail())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User already exist");
		}
		var user = userService.registration(registrationRequest.getEmail(), registrationRequest.getPassword(), registrationRequest.getLogin(), passwordEncoder);
		return AuthResponse.of(jwtProvider.generateToken(user), jwtProvider.generateRefreshToken(user));
	}

	private boolean emailExist(String email) {
		return userRepository.findByEmail(email).isPresent();
	}

	public AuthResponse performRefreshTokenPair(RefreshTokenRequest refreshTokenRequest) {
		var login = jwtProvider.getLoginFromToken(refreshTokenRequest.getRefreshToken());
		var user = userService.loadUserByUsername(login);
		return AuthResponse.of(jwtProvider.generateToken(user), jwtProvider.generateRefreshToken(user));
	}

	public AuthResponse performChangePassword(PasswordChangeRequest passwordChangeRequest) {
		if(passwordChangeRequest.getNewPassword() == null || passwordChangeRequest.getNewPassword().equals("")) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}
		User login = userService.loadUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		var user = userService.changePassword(login, passwordChangeRequest.getNewPassword(), passwordEncoder);
		return AuthResponse.of(jwtProvider.generateToken(user), jwtProvider.generateRefreshToken(user));
	}

	public AuthResponse performForgottenPasswordReplacement(ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
		var userLogin = jwtProvider.getLoginFromToken(forgottenPasswordReplacementRequest.getToken());
		var user = userService.loadUserByUsername(userLogin);

		var newUserDetails = userService.changePassword(user, forgottenPasswordReplacementRequest.getNewPassword(), passwordEncoder);
		return AuthResponse.of(jwtProvider.generateToken(newUserDetails), jwtProvider.generateRefreshToken(user));
	}

	public void performForgottenPasswordRequest(String email) {
		var user = userService.getUserByEmail(email);
		log.info(jwtProvider.generateToken(user));
	}
}

